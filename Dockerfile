FROM node:18-alpine

# Default values for env vars to pass build test
ARG REACT_APP_API_KEY=${REACT_APP_API_KEY}
ARG REACT_APP_PORT=${REACT_APP_PORT}
ARG REACT_APP_PROTOCOL=${REACT_APP_PROTOCOL}
ARG REACT_APP_HOST=${REACT_APP_HOST}

RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app
WORKDIR /home/node/app
USER node
COPY --chown=node:node . .
RUN npm install --force
RUN npm run build
EXPOSE 3000
CMD [ "npm", "run", "serve" ]
