// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require("prism-react-renderer/themes/github");
const darkCodeTheme = require("prism-react-renderer/themes/dracula");
require("dotenv").config();

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: "LEAF-VRE",
  tagline:
    "The Linked Editing Academic Framework - A Virtual Research Environment for the Humanities",
  url: "https://leaf-vre.org.com",
  baseUrl: "/",
  onBrokenLinks: "throw",
  onBrokenMarkdownLinks: "warn",
  favicon: "img/favicon.ico",

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: "LEAF", // Usually your GitHub org/user name.
  projectName: "LEAF VRE Website", // Usually your repo name.

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: "en",
    locales: ["en"],
  },

  presets: [
    [
      "classic",
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve("./sidebars.js"),
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          //editUrl:
          //   'https://gitlab.com/calincs/cwrc/leaf/leaf-vre-website/-/tree/main/docs',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          //editUrl:
          //  'https://gitlab.com/calincs/cwrc/leaf/leaf-vre-website/-/tree/main/blog',
        },
        theme: {
          customCss: require.resolve("./src/css/custom.css"),
        },
      }),
    ],
  ],
  themes: ["docusaurus-theme-search-typesense"],
  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      typesense: {
        typesenseCollectionName: "leaf-vre",
        typesenseServerConfig: {
          nodes: [
            {
              host: process.env.REACT_APP_HOST,
              port: process.env.REACT_APP_PORT,
              protocol: process.env.REACT_APP_PROTOCOL,
            },
          ],
          apiKey: process.env.REACT_APP_API_KEY,
        },
        // Optional: Typesense search parameters: https://typesense.org/docs/0.21.0/api/documents.md#search-parameters
        typesenseSearchParameters: {},
        // Optional
        contextualSearch: true,
      },
      navbar: {
        title: "Home",
        logo: {
          alt: "LEAF Logo",
          src: "img/logo.svg",
        },
        items: [
          {
            type: "doc",
            docId: "about-leaf/overview",
            position: "left",
            label: "About",
          },
          
          {
            type: "doc",
            docId: "/docs/documentation",
            position: "left",
            label: "Documentation",
          },
          {
            type: "doc",
            docId: "training/training",
            position: "left",
            label: "Training",
          },
          {
            type: "doc",
            docId: "features/leaf-commons",
            label: "Commons Functionality",
            className: "navbar-item",
            position: "right"
          },
          {
            type: "docSidebar",
            sidebarId: "functionalitySidebar",
            label: "VRE Functionality",
            className: "navbar-item",
            position: "right"
          },
          //{to: '/blog', label: 'Blog', position: 'left'},
          {
            href: "https://gitlab.com/calincs/cwrc/leaf/leaf-base-i8",
            label: "LEAF Codebase",
            position: "right",
          },
        ],
      },
      footer: {
        style: "dark",
        links: [
          {
            title: "Contact",
            items: [
              {
                label: "Email us",
                href: "mailto:leaf@leaf-vre.org",
              },
            ],
          },
          {
            title: "Community",
            items: [
              {
                label: "CWRC",
                href: "https://cwrc.ca",
              },
              {
                label: "Islandora Foundation",
                href: "https://islandora.ca/",
              },
              {
                label: "Twitter",
                href: "https://twitter.com/LEAF_VRE",
              },
            ],
          },
          {
            title: "Partners",
            items: [
              {
                label: "Bucknell University",
                href: "https://www.bucknell.edu/",
              },
              {
                label: "Newcastle University",
                href: "https://www.ncl.ac.uk/",
              },
              {
                label: "University of Guelph",
                href: "https://www.uoguelph.ca/",
              },
            ],
          },
          {
            title: "Sponsors",
            items: [
              {
                label: "CFI",
                href: "https://www.innovation.ca/",
              },
              {
                label: "CANARIE",
                href: "https://www.canarie.ca/",
              },
              {
                label: "Digital Research Alliance",
                href: "https://alliancecan.ca/en",
              },
            ],
          },
          {
            title: " ",
            items: [
              {
                label: "Mellon Foundation",
                href: "https://mellon.org/",
              },
              {
                label: "NEH",
                href: "https://www.neh.gov/",
              },
              {
                label: "SSHRC",
                href: "https://www.sshrc-crsh.gc.ca/home-accueil-eng.aspx",
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} LEAF-VRE. Built with Docusaurus.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
