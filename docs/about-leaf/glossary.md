---
sidebar_position: 7
sidebar_label: Glossary
---
# Glossary of Terms

If you want to learn more about a particular concept referred to throughout this site, as well as for more information about digital  knowledge production and linked open data, please check out the **[Glossary](https://lincsproject.ca/docs/learn-lod/glossary)** developed by our colleagues at the LINCS (Linked Infrastructure for Networked Cultural Scholarship) project. The glossary contains explanations of terms are used in LINCS’s documentation and other LOD resources. Clicking on a term brings up a longer definition with examples and links to further resources.