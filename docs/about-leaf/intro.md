---
sidebar_position: 1
sidebar_label: Intro
draft: true
---
# Intro to LEAF

![alt="LEAFlogo"](/img/about-leaf/LEAF-Logo-Color-Primary-FullName-large.png)

LEAF is an editorial environment for open collaboration and publication. It provides web-based tools and online spaces for more scholars, teachers, and students to take part in collaborative digital knowledge production: creating, publishing, preserving, linking and sharing data across the Web.

![alt="FAIR"](/img/about-leaf/FAIR_data_principles.svg)
LEAF supports Findable Accessible Interoperable Reusable (FAIR) data for the humanities, ensuring that digital scholarship can be preserved for future generations. It works to keep projects operational and accessible on the web.

LEAF is optimized for project support
Whether working on research, pedagogical, or community projects, LEAF supports researchers engaged in open and organized collaboration, allowing project editors to develop identities, assign roles and responsibilities, and manage workflows.

## LEAF is':'

- Open
- Accessible
- Sustainable
- Scalable
- Scholar-led
- Standards-based
- Dynamic, easily updatable
