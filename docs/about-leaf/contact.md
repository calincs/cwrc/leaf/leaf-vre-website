---
sidebar_label: "Contact Us"
sidebar_position: 9
title: "Contact Us"
---

import { faUpRightFromSquare } from "@fortawesome/free-solid-svg-icons";

Stay current with LEAF by joining the **LEAF-VRE Slack!** Join for announcements, support, and community conversation:

<div className="primary-button-row">
  
  <PrimaryButton
    link="https://forms.gle/YsUMKB1RtbXx5cPv5"
    buttonName="Join the LEAF-VRE Slack"
    icon={faUpRightFromSquare}></PrimaryButton>
 
</div>
<br/>

If you are interested in talking to us about how LEAF can work for you, installing a LEAF tool or LEAF-VRE in your environment, or collaborating with LEAF, please contact us at **leaf@leaf-vre.org**:

<div className="primary-button-row">
  
  <PrimaryButton
    link="mailto:leaf@leaf-vre.org"
    buttonName="Email Us"
    icon={faUpRightFromSquare}></PrimaryButton>
 
</div>
<br/>

**Follow us** (very intermittently) on X/Twitter:

<div className="primary-button-row">
  
  <PrimaryButton
    link="https://twitter.com/leaf_vre"
    buttonName="Follow Us on Twitter"
    icon={faUpRightFromSquare}></PrimaryButton>
</div>

We regularly provide **workshops and demonstrations**, both in person and online. Check out our **[Training page](training/workshops-and-presentations.md)** for upcoming Workshops and Presentations.
