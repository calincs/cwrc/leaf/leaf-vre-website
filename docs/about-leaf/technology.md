---
sidebar_position: 6
sidebar_label: Technology
---
# What makes LEAF work

LEAF is bringing together research data management technologies and editorial functionality that will allow researchers to store, prepare and publish digitized sources and scholarly output.

![alt="LEAFdiagram"](/img/about-leaf/LEAFsoftware_Diagram-detail.png)

See [Technical documentation](https://gitlab.com/calincs/cwrc/leaf/leaf-base-i8/-/wikis/home) for installation and code management instructions.
