---
sidebar_label: LEAF Commons
sidebar_position: 3
---

# LEAF Commons

**The LEAF Commons** provides a suite of interoperable tools to support textual scholarship, ranging from transcriptions or editions of digitized manuscripts or books to born-digital anthologies, collections, and long-form arguments. Publication is possible through multiple routes, and code repositories can be used for collaboration and hosting.
The LEAF Commons tools:

- [**LEAF-Writer**](features\about-lw.mdx) for rich semantic encoding of texts using the Text Encoding Initiative Guidelines, with the options to add linked data identifiers or web annotations.
- [**LEAF-Turning Engine**](features\leaf-te.md) for transforming texts for import or export from LEAF-Writer.
- [**Dynamic Table of Contexts**](features\dtoc.mdx) e-reader for publishing online interactive editions that unlock the semantic markup of a text to enhance findability, navigation, readability, and analysis.
- Coming soon: the improved **Named Entity Recognition Vetting Environment (NERVE)**

All Commons tools are open-source and designed to be easily adopted into other software stacks or extended to suit specific needs.
