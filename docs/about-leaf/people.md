---
sidebar_position: 5
sidebar_label: People
---
# People

LEAF is the fruit of a multiinstitutional, international collaboration.

## Leads

- [Diane Jakacki](https://orcid.org/0000-0002-7836-1223) (Bucknell University)
- [Susan Brown](https://orcid.org/0000-0002-0267-7344) (University of Guelph)
- [James Cummings](https://orcid.org/0000-0002-6686-3728) (Newcastle University)

## Staff

- Assistant director: Mihaela Ilovan
- Technical director: Jeff Antoniuk
- Senior Drupal Developer: Umed Singh
- Javascript developer and designer (LEAF-Writer & NERVE): [Luciano Dos Reis Frizzera](https://orcid.org/0000-0001-7244-4178)
- Research Associate: [Rachel Milio](https://orcid.org/0009-0000-0420-4711)
- Translation: Alice Hinchliffe
- JavaScript developer (DToC): Andrew MacDonald
- Pieter Botha: LINCS Technical Manager
- Wade Hutchison: Bucknell University Senior Cloud Systems Engineer
- Mark Turner: Newcastle University Research Software Engineer

## Past Staff

- Islandora consultant: Nigel Banks
- Design: Jennifer Blair
- Research Associate: Carolyn Black
- Drupal Developer: Nia Kathoni

## Partners

- [Linked Infrastructure for Networked Cultural Scholarship](https://lincsproject.ca)
- [Voyant Tools](https://voyant-tools.org/)
- Born Digital

LEAF has also received support from a number of [sponsors](docs/about-leaf/sponsors.md).
