---
sidebar_position: 1
sidebar_label: Overview
---
# Intro to LEAF

![alt="LEAFlogo"](/img/about-leaf/LEAF-Logo-Color-Primary-FullName-large.png)

LEAF is an editorial environment for open collaboration and publication. It provides web-based tools and online spaces for more scholars, teachers, and students to take part in collaborative digital knowledge production: creating, publishing, preserving, linking and sharing data across the Web.

![alt="FAIR"](/img/about-leaf/FAIR_data_principles.svg)

[Image](https://en.wikipedia.org/wiki/File:FAIR_data_principles.jpg) credit: Sangya Pundir, CC-BY-SA-4.0

LEAF tools promote FAIR data for the humanities and social sciences. The use of permanent identifiers helps make content findable and accessible. Standard data formats support interoperability and make content amenable to reuse and preservation. Suitable for research, pedagogical, or community projects, LEAF software supports individuals and groups, from novices to experts, in learning and applying best practices in digital scholarship through semantic technologies.

**The LEAF Commons** provides an open suite of interoperable tools for creating, editing, and publishing sophisticated digital scholarship. **The LEAF Virtual Research Environment (LEAF-VRE)** integrates LEAF tools with advanced content management and collaboration support to provide a research and publication platform for diverse projects.

<h2 className='homepageTable'>LEAF Commons Functionality</h2>
<table class='center'>
    <tr>
        <th>Create</th>
        <th>Manage</th>
        <th>Publish</th>
    </tr>
    <tr>
        <td>Digital Editions</td>
        <td>Document transcription</td>
        <td>Dynamic interface</td>
    </tr>
    <tr>
        <td>XML Editing in browser</td>
        <td>Transkribus import</td>
        <td>Leverage markup</td>
    </tr>
    <tr>
        <td>Named entity linking</td>
        <td>Import/local storage</td>
        <td>Curate editions</td>
    </tr>
    <tr>
        <td>Linked Data annotations</td>
        <td>Export/download</td>
        <td>Embed images</td>
    </tr>
    <tr>
        <td>Interactive eBooks</td>
        <td>Through Github</td>
        <td>Export as HTML</td>
    </tr>
    <tr>
        <td>Custom XML Styling</td>
        <td>Collaboration, Sharing</td>
        <td>Embed in websites</td>
    </tr>
    <tr>
        <td>HTML export</td>
        <td>Data versioning</td>
        <td>Embed in Wordpress</td>
    </tr>
</table>

<h2 className='homepageTable'>Integrated VRE Functionality</h2>
<table class='center'>
    <tr>
        <th>Create</th>
        <th>Manage</th>
        <th>Access</th>
    </tr>
    <tr>
        <td>Digital Editions</td>
        <td>Members</td>
        <td>Browse</td>
    </tr>
    <tr>
        <td>Thematic collections</td>
        <td>Roles</td>
        <td>Search</td>
    </tr>
    <tr>
        <td>Rich metadata</td>
        <td>Sharing</td>
        <td>Search by project</td>
    </tr>
    <tr>
        <td>XML editing in browser</td>
        <td>Communications</td>
        <td>XML-aware e-reader</td>
    </tr>
    <tr>
        <td>Linked data annotations</td>
        <td>Import/revise</td>
        <td>PDF viewer</td>
    </tr>
    <tr>
        <td>Bibliographies</td>
        <td>Export/download</td>
        <td>Image viewer</td>
    </tr>
    <tr>
        <td>Text from images (OCR)</td>
        <td>Backup/archiving</td>
        <td>Audio player</td>
    </tr>
    <tr>
        <td>Interactive e-books</td>
        <td>Object versioning</td>
        <td>Video player</td>
    </tr>
    <tr>
        <td>Custom XML schemas</td>
        <td>Document transcription</td>
        <td>Compound objects</td>
    </tr>
    <tr>
        <td>Custom XML stylesheets</td>
        <td>Project home pages</td>
        <td>User-curated editions</td>
    </tr>
</table>

