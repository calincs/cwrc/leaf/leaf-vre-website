---
sidebar_label: LEAF VRE
sidebar_position: 4
---

# LEAF Virtual Research Environment

**The LEAF Virtual Research Environment (LEAF-VRE)** platform allows for the hosting of images, audio, and video alongside text to enable multi-modal collections and exhibits across multiple projects connected by interoperable data formats and content interlinking. LEAF-VRE supports individual scholars and collaborative teams, allowing project editors to establish roles, assign tasks, and manage workflows for complex editorial and production processes.
        
* **Projects** with distinct landing, documentation pages, and dashboards.

* **Project collections** can be explored separately or discovered through platform-wide searches.

* **User profiles**; role and permissions management.

* **Workflow tracking** and management; task assignment and communications.

* **Batch processing**, e.g. ingest, OCR, downloads, exports for long-term preservation

* Integrated **LEAF-Writer** for web-based rich semantic encoding of texts using the Text Encoding Initiative Guidelines, with the options to add linked data identifiers or web annotations.

* Integrated **LEAF-Turning Engine** for transforming texts for import or export from LEAF-Writer.

* Integrated **Dynamic Table of Contexts** reader for publishing online interactive editions that unlock the semantic markup of a text to enhance findability, navigation, readability, and analysis.

LEAF-VRE customizes Islandora to enable digital humanities workflows and publication needs. Enhancements include an innovative web-based editing tool that allows users to employ TEI XML along with Linked Open Data Web Annotations to enhance discoverability and interoperability.

The LEAF-VRE is based on the [Islandora 2.0](https://www.drupal.org/project/islandora/releases/2.0.0) digital asset management framework. LEAF aims to address the challenges that face many who undertake and maintain large-scale collaborative digital humanities projects. It supports scalability, interoperability, and preservation while allowing for dynamic, iterative, and collaborative editing to ensure that our materials, collections, and editions will remain current, viable, and accessible.

LEAF-VRE and its component tools are offered as containerized open-source code, available for download and installation by other institutions. The VRE interface supports multiple languages (localization). LEAF is partnered with the Linked Infrastructure for Networked Cultural Scholarship ([LINCS](https://lincsproject.ca/)) cyberinfrastructure project. A forthcoming (2024) bridge for publishing LEAF metadata and semantic encoding as linked open data will advance the ecosystem for producing interoperable cultural data.
Current instances of LEAF-VRE:

* [LEAF at Bucknell](https://leaf.bucknell.edu/)
* Canadian Writing Research Collaboratory ([current](https://cwrc.ca/); LEAF-based site launch spring 2024)