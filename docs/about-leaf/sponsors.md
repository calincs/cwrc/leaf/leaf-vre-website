---
sidebar_position: 8
sidebar_label: Sponsors
---
# Sponsors

LEAF is made possible thanks to the work of many people as well as funding from our sponsors. 

<table class='sponsorTable'>
    <tr class='sponsorTable'>
        <td class='sponsorTable'><a href="https://alliancecan.ca/"><img src="/img/about-leaf/Alliance-logo.jpg" width="500" height="500" /></a></td>
        <td class='sponsorTable'><a href="https://www.canarie.ca/"><img src="/img/about-leaf/CANARIE-logo.jpg" width="500" height="500" /></a></td>
    </tr>
    <tr class='sponsorTable'>
        <td class='sponsorTable'><a href="https://www.innovation.ca/"><img src="/img/about-leaf/CFI-logo.jpg" width="500" height="500" /></a></td>
        <td class='sponsorTable'><a href="https://www.mellon.org/"><img src="/img/about-leaf/Mellon-logo.jpg" width="500" height="500" /></a></td>
    </tr>
     <tr class='sponsorTable'>
        <td class='sponsorTable'><a href="https://www.sshrc-crsh.gc.ca/"><img src="/img/about-leaf/SSHRC-logo.jpeg" width="700" height="500" /></a></td>
        <td class='sponsorTable'><a href="https://www.neh.gov/"><img src="/img/about-leaf/NEH-logo.jpg" width="400" height="400" /></a></td>
    </tr>
</table>














