---
sidebar_position: 2
sidebar_label: Description
draft: true
---
# Project Description

![alt="LEAFlogo"](/img/about-leaf/LEAF-Logo-Color-Primary-FullName-large.png)

The _Linked Academic Editing Framework (LEAF)_ is a Virtual Research Environment based on the [Islandora 2.0](https://www.drupal.org/project/islandora/releases/2.0.0)  digital asset management framework.
LEAF aims to address the challenges that face many who undertake and maintain large-scale collaborative digital humanities projects. It supports scalability, interoperability, and preservation while allowing for dynamic, iterative, and collaborative editing to ensure that our materials, collections, and editions will remain current, viable, and accessible.

LEAF is a collaboration to extend the [Canadian Writing Research Collaboratory](https://cwrc.ca)  (CWRC), built by the Universities of Alberta and Guelph, which launched in 2016. The LEAF platform combines hardware, software, and personnel. LEAF is being built on a solid foundation in its data models, core functionality, and code management, so that it is positioned for extension and long-term sustainability. The platform is based on the Islandora 2.0 framework, which combines Drupal 9 with a Fedora 5 repository for long-term preservation.

LEAF customizes and enhances Islandora to enable digital humanities workflows and publication needs. Enhancements include an innovative web-based editing tool that allows users to employ TEI XML along with Web Annotation and IIIF standards-compatible Linked Open Data annotations that enhance discoverability and interoperability. LEAF is offered as containerized open-source code, available for download and installation by other institutions. It will support the production and publication of dynamic digital scholarly editions and collections, offering multilingual transcription, translation, and image markup. Entirely browser-based, its functionality includes an in-browser XML markup editor (LEAF-Writer, built on CWRC-Writer, which can be installed as a standalone editing environment), XML rendering tools, built-in text and data visualization tools including the Voyant Tools suite and its Dynamic Table of Contexts Browser. LEAF will provide a sophisticated interface for digital content in which XML markup is leveraged for navigation and active reading, and enhanced with Linked Open Data. LEAF is partnered with the [Linked Infrastructure for Networked Cultural Scholarship](https://lincsproject.ca/) (LINCS) cyberinfrastructure project, as part of a larger linked open data ecosystem for research.
