---
sidebar_position: 3
sidebar_label: Related resources
draft: true
---
# Related resources

More tutorials coming soon!

Meanwhile, don't forgot to check out the LEAF [videos](https://www.youtube.com/@leaf-vre), upcoming [Workshops and Presentations](docs/training/workshops-and-presentations.md), and other related resources including:

* [Documentation](http://localhost:3000/docs/docs/documentation)
* Frequently Asked Questions ([FAQs](docs/documentation/faq))
* The LINCS [Glossary](https://lincsproject.ca/docs/get-started/glossary) of terms related to semantic encoding and linked open data.