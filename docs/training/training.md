---
sidebar_position: 1
sidebar_label: Training with LEAF
---
# Training with LEAF

The LEAF team is committed to supporting potential users through training and consultation: we want to help you find ways for LEAF and LEAF-Writer to work for your needs. We offer workshops online and in person.

## LEAF-Writer Commons

### LEAF-Writer

The standalone LEAF-Writer is a powerful semantic editor that is a great choice tool for editors who want to focus on the meaning in their texts rather than learn complex coding languages. It is web-based, so can be easily integrated into collaborative editorial workflows. And because it is free, with no usage fees or proprietary software to download, it is ideal for classroom use!

To get started with LEAF-Writer, check out [our video tutorials](docs/training/videos.mdx).

### Dynamic Table of Contexts

The Dynamic Table of Contexts transforms e-reading by combining the power of semantic markup with the navigational features of the book. The table of contents and the keyword index – the time-tested overviews provided by print editions – are dynamically merged with full-text search and tag-based indexing.

Check out the [DToC Tutorial](docs/training/tutorials/dtoc-tutorial.md).

### LEAF Platform

The feature-rich LEAF virtual research environment is a flexible platform designed for complex research and publication. It enables more scholars, teachers, and students to take part in digital knowledge production and collaboration, and promotes data linking and sharing across the web.

To get a sense of what LEAF can do, watch this [video](https://www.youtube.com/watch?v=jyCXRrfh5vk&t=6s), check out [LEAF at Bucknell](https://leaf.bucknell.edu/) and watch for the relaunch of CWRC this spring.

## Peer-training and consultation

Members of the LEAF team offer workshops at conferences and to small groups - in person and via teleconferencing. If you are interested in talking to one of us about how LEAF can work for you, contact us at [leaf@leaf-vre.org](mailto:leaf@leaf-vre.org).

