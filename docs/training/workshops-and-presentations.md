---
sidebar_position: 4
sidebar_label: Workshops and Presentations
---
# Workshops and Presentations

### Upcoming

- **LEAF Commons: Using Linked Tools for Digital Editorial Production** at [DH2024: Reinvention and Responsibility](https://dh2024.adho.org/) - Washington (in person) August 2024

- As part of [**Making Connections: The Semantic Web for Humanities Scholars**](https://www.uoguelph.ca/arts/research/centres-institutes-and-labs/digital-humanities-guelph/dh-events/summer-workshops/2024-6) at DH@Guelph Summer Workshop - Guelph (in person) 14-17 May 2024 

- **Editing and Publishing in TEI with LEAF-Writer** at [Forward Linking / DHSITE](https://hsscommons.ca/en/groups/forward_linking/events/2024conference/2024_workshops) - Ottawa (in person) 8 May 2024

**Stay tuned for more online workshops!**

### 2024

- Bucknell University LEAF-Writer Online Workshops - 5-part Series - January-February 2024

### 2023

- Modern Language Association Conference: San Francisco - January 2023
- Université de Montreal - February 2023
- DH@Guelph - 8 May 2023
- CSDH2023, Toronto, Canada - May 2023
- DHSI@Congress, Toronto, Canada - June 2023
- DHSI Atlantic, Cork, Ireland - June 2023
- DH2023: Graz, Austria - July 2023
  
### 2022

- DH2022: Tokyo, Japan - July 2022
- TEI 2022: Newcastle, UK - September 2022
- Bucknell University - December 2022