---
sidebar_position: 1
title: "VRE Functionality"
sidebar_class_name: hidden
pagination_next: null
---

<!-- @format -->

import FunctionalityCatalogue from '@site/src/components/functionalityCatalogue.js';

The LEAF Virtual Research Environment provides web-based tools and online spaces for researchers who want to create, publish, and preserve cultural and scholarly materials.

To learn more about LEAF VRE’s functionality, click on any of the cards below. You can also filter by particular type of function and learn which users have access to different functions. Information about specific tools and environments can be found under Features. To discover more about how to make the most of LEAF, see Documentation.

## VRE Functionality Catalogue

<FunctionalityCatalogue/>
