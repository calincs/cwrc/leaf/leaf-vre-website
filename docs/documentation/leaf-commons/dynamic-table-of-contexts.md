---
description: "Dynamic Table of Contexts Documentation"
title: "Dynamic Table of Contexts"
sidebar_position: 2
---

This is a brief documentation for the functions of DToC. For more on creating your own DToC, see [this tutorial](training\tutorials\dtoc-tutorial.md). 

#### What is DToC?
The Dynamic Table of Contexts (DToC) is an in-browser e-reader that uses the underlying structure of a document to allow different paths through the text to be discovered when read. The corpus may be navigated via semantic tags, the index (if present), or the document structure (such as headings). DToC allows readers to "curate" the text they are reading by permitting the customization of tags, even if the text being "read" is not their own.

DToC users can sign into their Github account to access documents stored in the cloud, as well as locally stored documents on your own device. 

#### Opening or Creating a DToC
A complete DToC has three components: a semantically-tagged, TEI compliant XML document, an index stored in an XML document, and a JSON file which informs DToC how to present your document. The index can be stored within the primary XML document, and your DToC can also be comprised of multiple XML documents. 

The JSON file is the backbone of a DToC. On the [DToC homepage](https://dtoc.leaf-vre.org/), the options for opening a DToC (From the Cloud, From URL, From Your Device, Paste) all require a JSON file. The final option, New, opens an Input Configuration Field where user inputs can generate a JSON file.

#### Required Fields for a DToC

The required fields for a DToC JSON file are the same regardless of whether the JSON is created elsewhere or generated via the Input Configuration Field. If uploading a DToC via JSON file, it is recommended to use [this template](https://github.com/LEAF-VRE/code_snippets/blob/main/DTOC/dtoc_template.json).

The “Title” field, or “editionTitle” in the JSON, is for the title of the entire DToC (not just the individual documents or sections).

The “Subtitle” field, or “editionSubtitle”, is for an optional subtitle. 

The Documents “URLs” field holds as many URLs as needed for the XML files the DToC will present. These links should be directly to the XML file of the text (such as via a raw Github file). In the JSON template, this is the “Inputs” field.

The remaining fields take an XPath input. For more about XPath, visit our supplemental XPath page. 

“Corpus Part” or “documents” denotes the individual documents which will appear in the table of contents.

Use “Part Content” if the text of each document part that you want to display is separate from the entirety of the document part.

Use “Part Title” or “documentTitle” for the field that should display as the title for each document.

Use “Part Author” or “documentAuthor” to display the author of each part.

Use “Corpus Index” or “indexDocument” to denote the part of your document(s) that contains an index.

Lastly, Curation “Markup” contains the TEI-XML tags you want to be displayed, along with their labels.

For more on setting up a DToC, see [DToC Step by Step guide](training\tutorials\dtoc-tutorial.md).

#### Navigating DToC
Once opened, the DToC interface has three panels. 

The left panel can be toggled between Index, which is the inputted index of terms, and Tags, which are the tags from the curated markup field. 

The index can contain subsections and cross-references to provide a robust keyword catalogue. The numbers located beside each term refer to the number of instances the term appears in the corpus.

The “Tags” tab displays any of the tags found in your XML document. If you use a URL or pasted-in text instead of an uploaded XML document, the “Tags” tab will be empty. The numbers next to the tags display how many times the tag appears in the document.

Clicking on an index term or tag displays their location in different ways. The Table of Contents expands, presenting the location of the term in each chapter or section, the Document Model highlights the lines that contain the term in the corpus, and the Reading Pane highlights the selected term in the readable text.

Index terms and Tags can be made simultaneously. Each of these will highlight sections of the text in a different colour, providing the ability to search for distinct patterns in the text.

To reset any search or filter option you can select the “Clear” button located at the top of the Document Model or unselect the chapter/section filter.

To search for terms in a specific section or chapter, you can choose to “Filter by Chapter.”
Clicking on the “Filter by Chapter” button yields a drop down list of the available sections. Once a particular chapter or section has been selected, the index terms will automatically be narrowed down.

The center panel is the Table of Contents. The Table of Contents presents the section or chapter breakdown of your document.
Each section is represented by the title(s) and author(s) specified in your XML document. Clicking on one of the sections or chapters will bring the text of the selected section into the Reading Pane.

When you select an option from the left-side column, the Table of Contents will display and link to the location of the selected term, tag, or word.

The Table of Contents also contains a search function. The search box is auto-complete enabled, but it does not recognize partial matches, that is to say, typing in the first few letters of a word and pressing enter will not provide a recognizable search term. You may, however, search for partial words by adding an asterisk at the end of the string of characters you have entered. Once a search is conducted, the different sections, or chapters, that contain the searched term will appear in the Table of Contents with brief excerpts of text surrounding the term in question. Lines in the document model, the narrow column to the right of the Table of Contents, will become highlighted to represent the location(s) of the term in the corpus. These highlighted lines may be clicked to bring the corresponding text into the Reader Pane. To search for a new term, delete your word in the “Word Search” box, or click on the “Clear” button located at the top of the document model, then type in or select a new term.
The right-hand panel is the Reading Pane. The text in the Reading Pane is separated into chapters, sections, or articles by the XML document. The Reading Pane loads individual sections; the entire document will not appear unless no text divisions have been embedded into the document.
