---
description: "LEAF-Writer Commons Intro"
title: "LEAF-Writer Commons Intro"
sidebar_position: 1
---

# LEAF-Writer Commons Documentation: Introduction

Note: This is meant to give an introductory understanding of LEAF-Writer's interface and functionality. For a more thorough understanding, see [LEAF-Writer Commons Documentation - Advanced](https://www.leaf-vre.org/docs/documentation/leaf-commons/leaf-writer-documentation). See the [LEAF-Writer Basics Step-by-Step Tutorial](https://www.leaf-vre.org/docs/training/tutorials/leaf-writer-basics-tutorial) to get started and learn about the fundamentals of markup..

## What is LEAF-Writer?

LEAF-Writer is a free web-based semantic encoding editor that allows you to mark up documents without installing proprietary software or learning how to code. With LEAF-Writer you can embed meaningful tags in texts so that they can be formatted, processed, analyzed, visualized, shared, and reused.

LEAF-Writer Commons allows you to open an XML file from your desktop or from the Cloud via a GitHub repository. You can also use one of LEAF-Writer’s genre-specific templates (Letter, Poem, Prose, Blank) to get started. When finished, you can export your file in XML, HTML, or Markdown format to do further work or publish your text on a website. [See LEAF Turning Engine Features](https://www.leaf-vre.org/docs/features/leaf-te)

## How to use LEAF-Writer?

To use LEAF-Writer you will need a laptop with internet access (at this point LEAF-Writer Commons is not optimized for handheld devices). It is compliant with all major browsers.

While you don't need a GitHub account to access LEAF-Writer, one is necessary in order to make full use of LEAF-Writer's capabilities, in particular being able to access and collaborate on documents in the cloud.
If you don't already have one, [go to GitHub](https://github.com/) for more information about setting up an account.

## Exploring LEAF-Writer

The LEAF-Writer interface is composed of a series of screens to help you make the most of your encoding experience.

### Opening screen

**Privacy Settings:** When you first access LEAF-Writer you will be asked to consider your Privacy Settings via a cookie notification banner. You can choose to accept all cookies, some, or none. If you choose 'reject all cookies' you can work locally in LEAF-Writer (uploading from and downloading to your desktop), but cannot access or share documents on GitHub ('in the cloud'). You can always adjust your cookie settings by clicking on the 'i' at the top right of this screen.

**Opening Documents:** By default you see action choices about opening a document from your desktop, importing and auto-transforming a document (currently output from the Transkribus HTR engine), experimenting with one of the available training documents, or getting started with one of the pre-loaded templates.

* Clicking on the 'open from your device' option will activate a pop-screen by which you can drag-and-drop or click to upload a valid TEI-XML document. See HERE to learn more about how you can prepare your document for upload to LEAF-Writer.
* You can also paste a full valid TEI-XML document into the clipboard, and it will open in LEAF-Writer. If the document is not well-formed or uses a schema that is not supported by LEAF-Writer, it will not open.
* If you have a GitHub account you can proceed to sign in. Once signed in (you'll know because your GitHub avatar appears in the top right of the screen) you will see an additional option to open 'From the Cloud'. Clicking on this option will activate a pop-up screen by which you can navigate your own GitHub repositories and those to which you have access.

### Workspace

After seeing a notification telling you that you are working in the Markup & Linking Editor mode (see HERE to learn more about different editor modes) you will access the LEAF-Writer editing workspace.

What follows is a quick tour of the workspace. See HERE for a more detailed tour. See the [LEAF-Writer Basics Step-by-Step Tutorial](https://www.leaf-vre.org/docs/training/tutorials/leaf-writer-basics-tutorial) to help you get started working in LEAF-Writer.

There are four main areas of the space that are designed to optimize your encoding experience:

* The **left panel**, including three tabs (Table of Contents, Markup, Entities)
* The **center editing space**, where you work on your text
* The **right panel**, including three tabs (Raw XML, Image Viewer, Validation)
* The **toolbar**, offering all of your tagging options (TEI elements, entities, translation) as well as various tools and settings.

**Additional functionality:**

* At the top left of the workspace screen, next to the LEAF-Writer logo, you'll see three horizontal lines. Clicking on this icon will activate a drop-down menu that allows you to create a new, open, or import a document, save or save a different version of your document, or export the document in XML, HTML, or Markdown format. See HERE for more information about importing and exporting documents with the LEAF Turning Engine.
* At the bottom left of the workspace screen are indicators that notify you about which editor and annotation mode you're working in, and which schema you are using for your document. See HERE for more information about schemas and LEAF-Writer.
* At the bottom right of the workspace screen, you'll see a button called 'Bugs/Requests' that will take you to an issue form on the LEAF-Writer GitLab repository.
* At the upper right of the workspace screen, next to your user avatar, you'll see a toggle button that allows you to change from editing to viewing mode. In viewing mode you'll notice that most of your functionality has disappeared so that you can share a read-only version of your encoded document.
