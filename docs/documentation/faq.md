---
title: "FAQs"
sidebar_position: 4
sidebar_label: "FAQs"
---
import { faUpRightFromSquare } from "@fortawesome/free-solid-svg-icons";

# Frequently Asked Questions

### The LEAF Environment

#### What’s the difference between LEAF-Writer Commons and LEAF-Writer as part of LEAF-VRE?

[LEAF-Writer Commons](https://leaf-writer.leaf-vre.org/) is a standalone instance of LEAF-Writer, but there is also a LEAF-Writer Drupal module which enables the use of LEAF-Writer inside the [LEAF-VRE system](https://gitlab.com/calincs/cwrc/leaf-writer/leaf_writer/-/blob/main/README.md). LEAF-VRE is based on the [Islandora 2.0](https://www.drupal.org/project/islandora/releases/2.0.0) digital asset management framework.

#### What’s the difference between CWRC and LEAF?

LEAF is an updated extension of the CWRC software. CWRC remains the name of the Canadian instance of LEAF.

#### Is LEAF-Writer internationalized?

Currently LEAF-Writer enables users to switch the interface language between English and French using the Language menu on the toolbar or in the Settings panel.

#### Is my data kept private in LEAF-Writer?

LEAF-Writer runs in the user’s browser using local storage and the LEAF-VRE team have no access to it. A cookie containing the user’s preferences for LEAF-Writer is stored in the browser.  The user is responsible for downloading local copies of the data or saving it in GitHub. These are not saved on the LEAF-VRE website.

### Using LEAF-Writer

#### Can I see the XML tags in LEAF-Writer?

LEAF-Writer exposes the XML being edited in various ways. There is a ‘Show Tags’ button which enables users to toggle on/off the basics tags in the editor view, and a ‘Raw XML’ panel on the right-hand side.  

#### What TEI elements does LEAF-Writer understand?

The elements available to you depend on the TEI schema you have selected. You may use any of these elements using the ‘Add Tag’ icon or right-click menu item. LEAF-Writer also provides handy shortcut icons (e.g. for encoding names and their associated entities) for: people, places, organizations, titles, things, citations, notes, dates, corrections, keywords, and links.

#### How can I check that my document is valid in LEAF-Writer?

Click the Validation button and this will open the right-hand Validation panel with an indication of whether your document is valid. If it is not valid you should change it. 

#### How can I see images alongside my TEI text in LEAF-Writer? 
On each Page Beginning `<pb/>` element in your TEI file, add a facs attribute which contains a URL pointing to the openly available page image for that page. These will then be displayed in the right-hand Image Viewer panel.

#### How can I provide a translation of a division in LEAF-Writer?

LEAF-Writer enables you to provide a translation of any division, by selecting that division in the markup panel and clicking the Translation button. This will prompt you for attributes such as language and responsibility, and provide a box to paste an initial (unencoded) translation into. The markup of the translation may be edited in the main LEAF-Writer editor once added.  

#### What are the differences between the editors modes in LEAF-Writer? 
There are a number of markup modes possible in LEAF-Writer. The default is ‘Marking and Linking XML tags and RDF’, and care should be taken in changing modes, as annotations may be lost.

The available modes are:
* **Markup only** – Any existing RDF annotations will be discarded and no RDF will be created when tagging entities.
* **Markup and Linking XML tags and RDF** – Semantic Web annotations equivalent to the XML tags will be created consistent with the hierarchy of the XML schema so annotations will not be allowed to overlap. (This is the default mode.)
* **Markup and Linking with overlap** – Only RDF will be created for entities that overlap existing XML structures
* **Linking Only** – There are plans for a Linking Only mode but this is not currently implemented, so it is greyed out

<div className="primary-button-row">
  
  <PrimaryButton
    link="https://www.leaf-vre.org/docs/documentation/leaf-writer-documentation#editor-modes"
    buttonName="Learn More about Editor Modes"
    icon={faUpRightFromSquare}></PrimaryButton>
 
</div>

#### What entity authorities are currently available, and can I add my own to LEAF-Writer? 
The current entity authorities built-in to LEAF-Writer are: the [Virtual International Authority File (VIAF)](https://viaf.org/), [Wikidata](https://www.wikidata.org), [DBpedia](https://www.dbpedia.org/), [Getty](https://www.getty.edu/research/tools/vocabularies/), [Geonames](https://www.geonames.org/), the [Lexicon of Greek Personal Names (LGPN)](https://www.lgpn.ox.ac.uk/), and [THE INTEGRATED AUTHORITY FILE (GND)](https://www.dnb.de/EN/Professionell/Standardisierung/GND/gnd_node.html.)

### LEAF-Writer Settings and Feedback

#### How can I change the LEAF-Writer appearance?
Users can modify the font-size of text and whether entities are highlighted via the Appearance menu in Settings. Overall appearance can be changed between Light, Auto, and Dark. Users can display or hide left and right-hand side panels.

#### Why aren’t my LEAF-Writer preferences consistent on all my machines? 
The LEAF-Writer settings are stored in individual browser instances as a cookie. The same user on a different machine will need to customize the LEAF-Writer interface to their own preferences. 

#### How can I control which authorities are preferred for which entity types in LEAF-Writer? 
In the LEAF-Writer settings individual users may easily customize which authorities are used for which entities and the order of preference by clicking on or off the individual icons or dragging the order of the authorities. 

#### How can I submit a bug report or feature request for LEAF-Writer? 
In LEAF-Writer users may click on ‘Bugs/Request’ to be taken to the project GitLab to submit an issue. You need to have, and be logged into, a GitLab account in order to submit an issue. See existing issues [here](https://gitlab.com/calincs/cwrc/leaf-writer/leaf-writer/-/issues/).

#### Can I change the default colors LEAF-Writer uses for various entities and elements? 
The only way to change the colors that LEAF-Writer uses is to specify your own schema and CSS. The schema URL can still point to one of the existing schema locations. 

### A Bit More Technical...

#### Do I have to use one of the provided TEI schemas or can I use my own customization?
So long as the  RELAX NG serialization of your customization of the TEI is valid and web-accessible (that is, can be located via a URL), you can associate it with your document and use it in LEAF-Writer. Click on the current schema name in the bottom-left and then clicking on the ‘+’ to add a new schema. 

#### Do I have to use the default CSS or can I use my own in LEAF-Writer?
You can use your own. So long as your CSS is valid and web-accessible (that is, can be located via a URL), you can associate it with your document and use it in LEAF-Writer. 

#### Can I associate a particular CSS with a particular schema in LEAF-Writer?
Yes, in LEAF-Writer when adding a schema (clicking on the current schema and then the ‘+’ to add a new one) it provides the opportunity to specify the name and URL of the schema as well as the default CSS for it.  
 
#### Can I edit the XML directly in LEAF-Writer? 
There is a right-hand side ‘Raw XML’ panel which shows the syntax-highlighted pretty-printed view of the current XML structures. This may then be edited by clicking ‘Edit’ which pops up a modal editor. This is primarily intended for small corrections while LEAF-Writer is being used to edit rather than as a full XML editor.

#### Can I add my own to entity authorities LEAF-Writer? 
It is not currently possible to add other authority files in the public instance of LEAF-Writer Commons, but this may be added in future versions. If there are important international authority files with robust APIs that you would like added to the ones found in LEAF-Writer, or you would like to collaborate on expanding the available authorities, please submit a feature request via [GitLab](https://gitlab.com/calincs/cwrc/leaf-writer/leaf-writer). 

#### Can I set up my own instance of LEAF-Writer?
Yes, but you should ask yourself if you really need to. The instance at [LEAF-Writer Commoms](https://leaf-writer.leaf-vre.org/) is freely available for public use. LEAF-Writer runs in the user’s browser and data is stored there (or wherever you save it, e.g. GitHub). However, if you really do want to set up your own instance of LEAF-Writer there is information on its installation available. 

<div className="primary-button-row">
  
  <PrimaryButton
    link="https://gitlab.com/calincs/cwrc/leaf-writer/leaf-writer/-/blob/main/readme.md"
    buttonName="Learn More about Installing LEAF-Writer"
    icon={faUpRightFromSquare}></PrimaryButton>
 
</div>


