---
description: "How to create Paged Content in LEAF"
title: "Paged Content"
draft: true
---
# How to Create a Paged Content Object

LEAF offers the Islandora "Paged Content" object model to present readable images of multi-paged books, periodicals, photographs, etc. using the International Image Interoperability Framework (IIIF) Mirador paginated viewer.  

Steps:

1. Create a repository item
2. Set model to Paged Content
3. Set type to collection
4. Set display hint to Mirador
5. Once the parent book is created, go to its "about" tab and click on edit children
6. Click on batch add children
7. Set content type to repository item
8. Set model to page
9. Set resource type to image
10. Set media usage to original
11. Upload multiple media (recommend batches)
12. Set alt text to each media
13. Save

Once you added the first children pages to the book object, go to the "about" tab of the book object and click on Media 》edit
Add a single image (the cover) as media and set media usage to thumbnail ONLy
Ingest that thumbnail and voila you have the book
Repeat steps (minus thumbnail) to add more pages)
