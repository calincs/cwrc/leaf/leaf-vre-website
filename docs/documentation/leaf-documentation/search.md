---
description: "How to Search within LEAF"
title: "Search"
---

<!-- @format -->

<head>
<script src="https://kit.fontawesome.com/2d8ee6f374.js" crossorigin="anonymous"></script>
</head>

import { faUpRightFromSquare } from "@fortawesome/free-solid-svg-icons";

# Search

LEAF utilizes a robust browse and search system to enhance access to all available materials and to encourage exploration.

The search bar is located at the top of all pages of the LEAF site. You can narrow down where in LEAF you are searching from the search bar by selecting either Collaboratory (the default option) or Website Content. If you would like more search options, there is an Advanced Search link to the right of the search box.

# Search Results
Search Results can be sorted by the relevance to search terms, title, creation date, and date last modified. These options can be organized in ascending and descending order and/or list (default) or grid view.

Users can also view search results by Bibliographic View, which displays search results as citations. These can be organized by type of citation style:

Another available view of "search results" is by Documents. This allows users to view the search results as “abstracts.” The information presented includes the latest workflow stamp, title, author, description, etc. However, this view depends on the metadata and not all files have all those fields filled in.

# Advanced Search
The Advanced search option allows users to enter multiple search terms (separated by boolean operators - and, or, and not). You can have from one to multiple search terms. The red “+” and “-” buttons beside the search terms will add or delete them. You can specify where the search term should be found by changing the Field option.

The Field list narrows down your search by the metadata and workflow stamps of objects. The default option is dc.title, which will return the most results because it is a required field for new objects.

# Searching Results
There are multiple ways to filter search results in LEAF. You can travel through the accessible projects sorting them by title.

Additionally, LEAF provides a faceted browsing feature. See Facets for more information.
