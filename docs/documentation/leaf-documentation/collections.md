---
description: "How to manage collections within LEAF"
title: "Collections"
---

<!-- @format -->

<head>
<script src="https://kit.fontawesome.com/2d8ee6f374.js" crossorigin="anonymous"></script>
</head>

import { faUpRightFromSquare } from "@fortawesome/free-solid-svg-icons";

## Create a Collection

To create a Collection within a Project (or user workspace), go to the "Nodes" tab

1. Choose "Add New Content"
2. Choose "Group node (Repository Item)"
3. Fill out the pertinent metadata fields, making sure that:

   1. From "Resource Type" choose "Collection" from the dropdown menu
   2. From "System/Model" choose "Collection" from the dropdown menu

4. Save

## Add Items to a Collection

  A Repository Item can be added to a Collection in one of two ways:

  1. When creating a Repository Item: when filling out metadata fields look for System/Member of.
     1. Type the name of the Collection and Save.
  2. From within the Collection in "About/Specifications" choose "Edit" next to Pages
     1. Choose "Add Child" and then fill in the metadata fields as you would for any other Repository Item.

## Publish a Collection

By default Collections are 'published'. If you want to make a Collection private, edit the Collection repository item and uncheck the "Published" checkbox.

## Notes

+ A Collection can only contain repository items (includingotherCollections). A Basic Page cannot be contained by a Collection.
+ A Collection is 'owned' by one user, but depending on permissions items can be added to a Collection by any person who has permission to do so
+ A Collection (and the repository items it contains) cannot belong to multiple projects/groups, but a Collection can be transferred from one project to another by users who have permissions to do so.
