---
description: "Types of Content within LEAF"
title: "Content"
draft: true
---

<!-- @format -->

<head>
<script src="https://kit.fontawesome.com/2d8ee6f374.js" crossorigin="anonymous"></script>
</head>

import { faUpRightFromSquare } from "@fortawesome/free-solid-svg-icons";

## <i className="fa-solid fa-person-digging"></i> **This page is under construction.** <i className="fa-solid fa-person-digging"></i>

# Content
Create and edit text, image, and multimedia items

- Create/update item media
- Uploading single objects
- Uploading multipage objects
- Bulk Upload
