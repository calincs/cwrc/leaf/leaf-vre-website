---
sidebar_position: 3
sidebar_label: LEAF Turning Engine
---

# The LEAF Turning Engine

_An easier way to transform files and optimize workflow._

The LEAF Turning Engine (LEAF-TE) runs as part of LEAF-Writer Commons and is currently being integrated with the wider XML document editing functionality in the LEAF virtual research environment.

LEAF-TE provides functionality for people to edit machine-readable texts in TEI, and then publish those texts in a variety of output format.
This work is done using eXtensible Stylesheet Language Transformations (XSLTs) - scripts that activate transformation scenarios (e.g. 'turn this Transkribus output file into valid TEI and run'). Often, a user needs to learn how to write or adapt XSLTs and then process them in a code editor like Oxygen. LEAF-TE makes this easier for users who just want a simple file transformation to get on with their editorial work.

Running LEAF-TE is simple: just import or export a file from LEAF-Writer.

In LEAF-Writer Commons, choose "Import document". Currently you will see one option:  "Transkribus". Drag your Transkribus file onto the box or upload it from your desktop. The file should automatically be transformed and open as a TEI file in LEAF-Writer.

When you are finished encoding your file, you can choose to export a TEI-XML or HTML version of the file. You can do further work on the TEI file in LEAF-Writer or another code editor, or you can host the HTML file on a website or embed it in a content management system like Wordpress or Drupal.

The LEAF team is in the process of expanding LEAF-TE's functionality to include other HTR/OCR formats plus MS Word file import and Markdown, JSON, and Text export formats.

The code that runs LEAF-TE, as well as the XSLT files, is freely available for use. If your transformations are more complex than LEAF-TE supports, feel free to make a copy and alter them for your own use in Oxygen or another environment.

LEAF-TE is in active development. If you encounter a bug, or have a feature request you would like us to consider implementing, put in a [ticket](https://gitlab.com/calincs/cwrc/leaf/turning-engine/-/issues) in the LEAF GitLab project, and someone will respond as soon as possible.
