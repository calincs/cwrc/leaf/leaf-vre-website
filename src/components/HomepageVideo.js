import React from 'react';
import ReactPlayer from 'react-player/youtube';

const HomepageVideo =(props) =>{
return(
    <div className='videoContainer'>

        <ReactPlayer url={props.url}/>
        </div>
    
 );
}

export default HomepageVideo;