import React from "react";
import Functionality from "@site/static/functionality.json";
import FunctionalityCard from "./cards/FunctionalityCard";
import buttonStyles from "../components/buttons/buttons.module.css";
import styles from "./functionalityCatalogue.module.css";
import { useState, useEffect } from "react";
import functionTypes from "./functions";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faPeopleRoof,
  faInbox,
  faUpload,
  faChalkboardUser,
  faMagnifyingGlass,
  faFileCirclePlus,
  faEye,
  faBookBookmark
} from "@fortawesome/free-solid-svg-icons";

library.add(
    faPeopleRoof,
    faInbox,
    faUpload,
    faChalkboardUser,
    faMagnifyingGlass,
    faFileCirclePlus,
    faEye,
    faBookBookmark
);


function FunctionalityFilters({ activeFilters, setActiveFilters }) {
  const filterRoles = [
    "Public",
    "Member",
    "Contributor",
    "Project Editor",
    "Technical Editor", 
    "Workspace Editor"
  ];

  const handleClick = (filter) => {
    let _activeFilters = [...activeFilters];
    // Resetting filter
    if (activeFilters.includes(filter)) {
      _activeFilters = _activeFilters.filter((item) => item !== filter);
    } else {
      _activeFilters = _activeFilters.filter((item) => item !== "All");
      _activeFilters.push(filter);
    }

    //Resets and sets filters
    if (_activeFilters.length === 0) {
      setActiveFilters(["All"]);
    } else {
      setActiveFilters(_activeFilters);
    }
  };

  return (
    <div className={styles.filters}>
        <p className={styles["filter-label"]}>Filter by Functionality
        </p>
      <div className="function-button-row">
        {/* generates function filter buttons */}
        {Object.keys(functionTypes).map((filter) => (
          <button
            key={filter}
            className={`${activeFilters.includes(filter)
              ? [buttonStyles.functionButton, styles.filterButton].join(" ")
              : buttonStyles.functionButton
              } `}
            onClick={() => handleClick(filter)}
          >
            <div>
              <FontAwesomeIcon icon={functionTypes[filter].icon} />
            </div>
            <div className={styles.functionButtonName}>
              {functionTypes[filter].id}
            </div>
          </button>
        ))}
      </div>

        <p className={styles["filter-label"]}>Filter by User Role</p>
      <div className="function-button-row">
        {/* generates user role filter buttons */}
        {filterRoles.map((filter) => (
          <button
            key={filter}
            className={`${activeFilters.includes(filter)
              ? [styles.filterButton, buttonStyles.functionButton].join(" ")
              : buttonStyles.functionButton
              } `}
            onClick={() => handleClick(filter)}
          >
            <div className={styles.functionButtonName}>{filter}</div>
          </button>
        ))}
      </div>
    </div>
  );
}

const FunctionalityList = ({ functionality, resetFilter, resetQuery }) => {
  if (functionality.length === 0) {
    return (
      <div className={styles["empty-list"]}>
        <p>No Functionality Found!</p>
        <button
          className={buttonStyles.primaryButton}
          onClick={() => {
            resetFilter(["All"]);
            resetQuery("");
          }}
        >Reset Filters</button>
      </div>
    );
  }

  return (
    <div className={styles["functionality-list"]}>
      {functionality.map((x) => (
        <FunctionalityCard
          key={x["Functionality Name"]}
          {...{
            functionality: x["Functionality Name"],
            image: x["Image Path"],
            description: x["Description"],
            path: x["Path"],
            functions: x["Functions"],
          }}
        ></FunctionalityCard>
      ))}
    </div>
  );
};

function FunctionalityCatalogue({ preFilter = "All" }) {
  const [items, setItems] = useState([]);
  const [query, setQuery] = useState("");
  const [searchParam] = useState([
    "Functionality Name",
    "Description",
    "Functions",
    "Keywords",
    "Role",
  ]);
  const [filters, setFilters] = useState(Array.isArray(preFilter) ? preFilter : [preFilter]);

  useEffect(() => {
    setItems(search(Functionality));
  }, []);

  const search = (items) => {
    return items.filter((item) => {
      // Checking both lvl and fx filters for a match
      if (
        filters.every(
          (filt) => item.Functions.includes(filt) || item.Role.includes(filt)
        )
      ) {
        // Looking for exact string match, may want to change this to a more forgivable regex
        return searchParam.some((newItem) => {
          return (
            item[newItem]
              .toString()
              .toLowerCase()
              .indexOf(query.toLowerCase()) > -1
          );
        });
      } else if (filters == "All") {
        return searchParam.some((newItem) => {
          return (
            item[newItem]
              .toString()
              .toLowerCase()
              .indexOf(query.toLowerCase()) > -1
          );
        });
      }
    });
  };

  // Allows list to be prefiltered to be used on other pages
  // REVIEW: is it desirable to have input field and filters visible
  return preFilter != "All" ? (
    <FunctionalityList functionality={search(items)} />
  ) : (
    <>
      <FunctionalityFilters
        setActiveFilters={setFilters}
        activeFilters={filters}
      ></FunctionalityFilters>

      <input
        type="search"
        className={styles["functionality-search"]}
        placeholder={"Search by title or keyword" }
        value={query}
        onChange={(e) => setQuery(e.target.value)}
      ></input>
      <FunctionalityList
        functionality={search(items)}
        resetFilter={setFilters}
        resetQuery={setQuery}
      />
    </>
  );
}

export default FunctionalityCatalogue