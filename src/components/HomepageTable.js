import React from 'react';

const HomepageTable =(props) =>{
return(
    <div className="homepageTable">
<table>
      
      <tr>
        <td>
          <li>Open-Source</li>
          <li>Accessible</li>
          <li>Scalable</li>
          <li>Standards-Based</li>
          <li>Scholar-Led</li>
          <li>Designed for Extensibility and Sustainability</li>
        </td>
      </tr>
    </table>
       
        </div>
    
 );
}

export default HomepageTable;