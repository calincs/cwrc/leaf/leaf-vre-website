import React from "react";

const functionTypes = {
  CreateEdit: {
    id: "Create/Edit",
    link: "/docs/functionality",
    icon: "fa-file-circle-plus",
  },
  FindView: {
    id: "Find/View",
    link: "/docs/functionality",
    icon: "fa-magnifying-glass",
  },
  ManageAccounts: {
    id: "Manage Accounts",
    link: "/docs/functionality",
    icon: "fa-people-roof",
  },
  Organize: {
    id: "Organize",
    link: "/docs/functionality",
    icon: "fa-inbox",
  },
  Publish: {
    id: "Publish",
    link: "/docs/functionality",
    icon: "fa-book-bookmark",
  },
  Review: {
    id: "Review",
    link: "/docs/functionality",
    icon: "fa-eye",
  },
  Teach: {
    id: "Teach",
    link: "/docs/functionality",
    icon: "fa-chalkboard-user",
  },
};

export default functionTypes;