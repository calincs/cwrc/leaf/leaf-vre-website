import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
    {
    title: 'Flexible Collaboration Software',
    Svg: require('@site/static/img/Dashboard-cropped_adobe_express.svg').default,
    description: (
      <>
        LEAF provides open software to enable complex editorial workflows and collaborative research. 
      </>
    ),
  },
  {
    title: 'Scholarly Publishing Commons',
    Svg: require('@site/static/img/publishing_adobe_express.svg').default,
    description: (
      <>
        A suite of stand-alone tools for scholarly editing and publishing, accessible in your browser. 
      </>
    ),
  },
  {
    title: 'Virtual Research Environment',
    Svg: require('@site/static/img/VRE_adobe_express.svg').default,
    description: (
      <>
        Integrated support for creation and management of online editions, collections, and exhibits.
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
