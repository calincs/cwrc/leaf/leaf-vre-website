/** @format */

import React from "react";
import Link from "@docusaurus/Link";
import styles from "./cards.module.css";
import functionTypes from "../functions";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faPeopleRoof,
  faInbox,
  faUpload,
  faChalkboardUser,
  faMagnifyingGlass,
  faFileCirclePlus,
  faEye,
} from "@fortawesome/free-solid-svg-icons";

library.add(
    faPeopleRoof,
    faInbox,
    faUpload,
    faChalkboardUser,
    faMagnifyingGlass,
    faFileCirclePlus,
    faEye,
);

const MiniFunctionButton = ({ name }) => {
  return (
    <Link
      className={styles["functionality-card-buttons"]}
      to={functionTypes[name].link}
      title={name}>
      <div title={name}>
        <FontAwesomeIcon title={name} icon={functionTypes[name].icon} />
      </div>
    </Link>
  );
};

function FunctionalityCard({ functionality, image, description, path, functions }) {
  return (
    <div className={styles["functionality-card"]}>
      <div className={styles["title"]}>
        <Link to={path}>
          <h4>{functionality}</h4>
        </Link>
      </div>

      <Link to={path}>
        <div className={styles["image"]}>
          {image ? <img src={"/img/" + image} /> : null}
        </div>
      </Link>
      <div className={styles["body"]}>
        <p>{description}</p>
      </div>

      <div className={styles["footer"]}>
        <div className={styles["functionality-function-button-row"]}>
          {functions.map((x) => (
            <MiniFunctionButton key={x} name={x} />
          ))}
        </div>
      </div>
    </div>
  );
}

export default FunctionalityCard;
