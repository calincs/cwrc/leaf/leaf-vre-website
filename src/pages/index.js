import React from 'react';
import clsx from 'clsx';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import Layout from '@theme/Layout';
import HomepageFeatures from '@site/src/components/HomepageFeatures';

import styles from './index.module.css';
import HomepageVideo from '../components/HomepageVideo';
import HomepageTable from '../components/HomepageTable';

function HomepageHeader() {
  const {siteConfig} = useDocusaurusContext();
  return (
    <header className={clsx('hero hero--primary', styles.heroBanner)}>
      <div className="container">
        <h1 className="hero__title">{siteConfig.title}</h1>
        <p className="hero__subtitle">{siteConfig.tagline}</p>
        <div className={styles.buttons}>
          <Link
            className="button button--secondary button--lg"
            to="/docs/about-leaf/overview">
            Learn more
          </Link>
        </div>
      </div>
    </header>
  );
}

export default function Home() {
  const {siteConfig} = useDocusaurusContext();
  return (
    <Layout
      title={`Home`}
      description="Documentation site for the LEAF project">
      <HomepageHeader />
      <main>
        <HomepageFeatures />
        <h2 className='videoContainer' >Brief Introduction to LEAF</h2>
        <HomepageVideo url="https://youtu.be/jyCXRrfh5vk"/>
        <h2 className='homepageTable'>LEAF is:</h2>
        <HomepageTable/>
      </main>
    </Layout>
  );
}
