import MDXComponents from "@theme-original/MDXComponents";

import PrimaryButton from "@site/src/components/buttons/PrimaryButton";
import FunctionButton from "@site/src/components/buttons/FunctionButton";


export default {
  ...MDXComponents,
  PrimaryButton,
  FunctionButton,
};
