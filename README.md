# Website

This website is built using [Docusaurus 2](https://docusaurus.io/), a modern static website generator.

## Installation

You will need Git (with an ssh key set up to connect to GitLab), NodeJS (v16 or newer), and VSCode to develop this website.

```bash
git clone git@gitlab.com:calincs/cwrc/leaf/leaf-vre-website.git
cd leaf-vre-website
```

## Local Development

```bash
npm install
npm start
```

This command starts a local development server at `http://localhost:3000/` and opens up a browser window. Most changes are reflected live without having to restart the server.

## Deployment

Commit and push the changes back to the upstream repo on GitLab.
