# Online Workshop: TEI Semantic Encoding and LEAF-Writer – Jan. 23, 4-5:30pm (et)

LEAF-Writer Online Workshop - January 23, 2024
Online 90-minute workshop 4-5:30 (real time)
Facilitated by Diane Jakacki, Bucknell University

This is a hands-on online workshop to introduce textual researchers to [LEAF-Writer](https://leaf-writer.leaf-vre.org/), the free web-based, semantic code editor that supports text encoding and annotation without users having to learn complex coding languages.

In this hands-on workshop participants will be introduced to the concepts of semantic and scholarly editing and annotation using Digital Humanities standards and best practices using LEAF-Writer, learning how to support work in semantic markup, add inline scholarly notes and glosses, and create annotations - tagging named entities and associating them with recognized authorities to provide connections between your work and that of other researchers on the Semantic Web.  

Working with supplied materials, participants will be able to experiment with texts in a number of genres, including prose, verse, and documentary archival materials.

Requirements: participants will need a web-enabled computer (not a tablet or a mobile device). No software installation or configuration is required. The workshop will be conducted on Zoom. The workshop will be recorded and posted for those who cannot attend in real-time.

Participation is free, but space is limited. For planning purposes please register [here](https://forms.gle/AX5JkmtxpMTvBPNp6). Once registered you will receive a confirmation email including a Zoom link and instructions about accessing materials for the workshop.

For more information, please contact Diane Jakacki: dkj004@bucknell.edu

LEAF-Writer is a core component of the [Linked Editing Academic Framework](https://www.leaf-vre.org/) virtual research environment. LEAF-Writer’s ongoing development and access are made possible through generous funding from the Mellon Foundation, the Canada Foundation for Innovation, the National Endowment for the Humanities, and the Social Sciences and Humanities Research Council, and support from Bucknell University, the University of Guelph, and the University of Alberta.
